# Google Sheet GUI
A simple .NET GUI with CRUD Operations for Google Sheet Documents

## After cloning / on first use
- Go to [Google Sheet API](https://console.developers.google.com/apis/api/sheets.googleapis.com/) and activate the Google Sheet API.
- Go to [Creadentials Tab](https://console.developers.google.com/apis/api/sheets.googleapis.com/credentials) and create an OAuth-Client-ID (Type: Others).
- Download the credentials file and name it `credentials.json`.
- Put the `credentials.json` file in folder `auth` folder where the executable is (if there is no auth folder, create it!).