﻿/* GoogleSheetGUI, NewCellData.cs
 * Holds information of one cell
 * Kai Sackl, 18.02.2020
 */

namespace GoogleSheetGUI
{
    public class NewCellData
    {
        public int SheetId { get; }
        public int StartRowIndex { get; }
        public int EndRowIndex { get; }
        public int StartColumnIndex { get; }
        public int EndColumnIndex { get; }
        public string Value { get; }

        public NewCellData(int sheetId, int startRowIndex, int startColumnIndex, string value)
        {
            SheetId = sheetId;
            StartRowIndex = startRowIndex + 1; // +1 -> Exclude row with column names
            EndRowIndex = StartRowIndex + 1;
            StartColumnIndex = startColumnIndex;
            EndColumnIndex = StartColumnIndex + 1;
            Value = value;
        }
    }
}
