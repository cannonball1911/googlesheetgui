﻿using System.Collections.Generic;
using Google.Apis.Sheets.v4;

/* GoogleSheetGUI, Globals.cs
 * Contains some globals
 * Kai Sackl, 19.02.2020
 */

namespace GoogleSheetGUI
{
    internal static class Globals
    {
        internal static int NewRowsCount;
        internal static bool DgvPopulated;
        internal static List<UserSpreadsheet> SpreadsheetList;
        internal static List<NewCellData> NewCellDatas;

        internal static class Google
        {
            internal static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
            internal static readonly string ApplicationName = "Google Sheet GUI";
            internal static SheetsService Service;
        }
    }
}
