﻿namespace GoogleSheetGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnFetchSheet = new System.Windows.Forms.Button();
            this.DgvSheet = new System.Windows.Forms.DataGridView();
            this.LblSearch = new System.Windows.Forms.Label();
            this.TbxSearch = new System.Windows.Forms.TextBox();
            this.TbxSpreadsheetID = new KaisLib.Controls.WatermarkedTextBox();
            this.TbxRange = new KaisLib.Controls.WatermarkedTextBox();
            this.LblEntries = new System.Windows.Forms.Label();
            this.BtnPushToSheet = new System.Windows.Forms.Button();
            this.CbxSpreadsheet = new System.Windows.Forms.ComboBox();
            this.BtnSearch = new System.Windows.Forms.Button();
            this.BtnResetSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DgvSheet)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnFetchSheet
            // 
            this.BtnFetchSheet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFetchSheet.Location = new System.Drawing.Point(698, 37);
            this.BtnFetchSheet.Name = "BtnFetchSheet";
            this.BtnFetchSheet.Size = new System.Drawing.Size(90, 23);
            this.BtnFetchSheet.TabIndex = 0;
            this.BtnFetchSheet.Text = "Fetch Sheet";
            this.BtnFetchSheet.UseVisualStyleBackColor = true;
            this.BtnFetchSheet.Click += new System.EventHandler(this.BtnGetData_Click);
            // 
            // DgvSheet
            // 
            this.DgvSheet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvSheet.Location = new System.Drawing.Point(12, 64);
            this.DgvSheet.Name = "DgvSheet";
            this.DgvSheet.Size = new System.Drawing.Size(776, 368);
            this.DgvSheet.TabIndex = 1;
            this.DgvSheet.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvSheet_CellValueChanged);
            // 
            // LblSearch
            // 
            this.LblSearch.AutoSize = true;
            this.LblSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSearch.Location = new System.Drawing.Point(13, 475);
            this.LblSearch.Name = "LblSearch";
            this.LblSearch.Size = new System.Drawing.Size(51, 16);
            this.LblSearch.TabIndex = 2;
            this.LblSearch.Text = "Search";
            // 
            // TbxSearch
            // 
            this.TbxSearch.Location = new System.Drawing.Point(12, 494);
            this.TbxSearch.Name = "TbxSearch";
            this.TbxSearch.Size = new System.Drawing.Size(232, 20);
            this.TbxSearch.TabIndex = 3;
            this.TbxSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbxSearch_KeyPress);
            // 
            // TbxSpreadsheetID
            // 
            this.TbxSpreadsheetID.Location = new System.Drawing.Point(12, 38);
            this.TbxSpreadsheetID.Name = "TbxSpreadsheetID";
            this.TbxSpreadsheetID.Size = new System.Drawing.Size(344, 20);
            this.TbxSpreadsheetID.TabIndex = 4;
            this.TbxSpreadsheetID.WaterMark = "Spreadsheet ID";
            this.TbxSpreadsheetID.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.TbxSpreadsheetID.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbxSpreadsheetID.WaterMarkForeColor = System.Drawing.Color.LightGray;
            // 
            // TbxRange
            // 
            this.TbxRange.Location = new System.Drawing.Point(362, 38);
            this.TbxRange.Name = "TbxRange";
            this.TbxRange.Size = new System.Drawing.Size(330, 20);
            this.TbxRange.TabIndex = 5;
            this.TbxRange.WaterMark = "Fetch Range";
            this.TbxRange.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.TbxRange.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbxRange.WaterMarkForeColor = System.Drawing.Color.LightGray;
            // 
            // LblEntries
            // 
            this.LblEntries.AutoSize = true;
            this.LblEntries.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEntries.Location = new System.Drawing.Point(12, 435);
            this.LblEntries.Name = "LblEntries";
            this.LblEntries.Size = new System.Drawing.Size(52, 16);
            this.LblEntries.TabIndex = 6;
            this.LblEntries.Text = "Entries:";
            // 
            // BtnPushToSheet
            // 
            this.BtnPushToSheet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPushToSheet.Location = new System.Drawing.Point(642, 438);
            this.BtnPushToSheet.Name = "BtnPushToSheet";
            this.BtnPushToSheet.Size = new System.Drawing.Size(146, 23);
            this.BtnPushToSheet.TabIndex = 7;
            this.BtnPushToSheet.Text = "Push all to spreadsheet";
            this.BtnPushToSheet.UseVisualStyleBackColor = true;
            this.BtnPushToSheet.Click += new System.EventHandler(this.BtnPushToSheet_Click);
            // 
            // CbxSpreadsheet
            // 
            this.CbxSpreadsheet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxSpreadsheet.FormattingEnabled = true;
            this.CbxSpreadsheet.Location = new System.Drawing.Point(12, 11);
            this.CbxSpreadsheet.Name = "CbxSpreadsheet";
            this.CbxSpreadsheet.Size = new System.Drawing.Size(344, 21);
            this.CbxSpreadsheet.TabIndex = 8;
            this.CbxSpreadsheet.SelectedIndexChanged += new System.EventHandler(this.CbxSpreadsheet_SelectedIndexChanged);
            // 
            // BtnSearch
            // 
            this.BtnSearch.Location = new System.Drawing.Point(250, 493);
            this.BtnSearch.Name = "BtnSearch";
            this.BtnSearch.Size = new System.Drawing.Size(75, 23);
            this.BtnSearch.TabIndex = 9;
            this.BtnSearch.Text = "Search";
            this.BtnSearch.UseVisualStyleBackColor = true;
            this.BtnSearch.Click += new System.EventHandler(this.BtnSearch_Click);
            // 
            // BtnResetSearch
            // 
            this.BtnResetSearch.Location = new System.Drawing.Point(329, 493);
            this.BtnResetSearch.Name = "BtnResetSearch";
            this.BtnResetSearch.Size = new System.Drawing.Size(75, 23);
            this.BtnResetSearch.TabIndex = 10;
            this.BtnResetSearch.Text = "Reset";
            this.BtnResetSearch.UseVisualStyleBackColor = true;
            this.BtnResetSearch.Visible = false;
            this.BtnResetSearch.Click += new System.EventHandler(this.BtnResetSearch_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 526);
            this.Controls.Add(this.BtnResetSearch);
            this.Controls.Add(this.BtnSearch);
            this.Controls.Add(this.CbxSpreadsheet);
            this.Controls.Add(this.BtnPushToSheet);
            this.Controls.Add(this.LblEntries);
            this.Controls.Add(this.TbxRange);
            this.Controls.Add(this.TbxSpreadsheetID);
            this.Controls.Add(this.TbxSearch);
            this.Controls.Add(this.LblSearch);
            this.Controls.Add(this.DgvSheet);
            this.Controls.Add(this.BtnFetchSheet);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "GoogleSheetGUI";
            ((System.ComponentModel.ISupportInitialize)(this.DgvSheet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnFetchSheet;
        private System.Windows.Forms.DataGridView DgvSheet;
        private System.Windows.Forms.Label LblSearch;
        private System.Windows.Forms.TextBox TbxSearch;
        private KaisLib.Controls.WatermarkedTextBox TbxSpreadsheetID;
        private KaisLib.Controls.WatermarkedTextBox TbxRange;
        private System.Windows.Forms.Label LblEntries;
        private System.Windows.Forms.Button BtnPushToSheet;
        private System.Windows.Forms.ComboBox CbxSpreadsheet;
        private System.Windows.Forms.Button BtnSearch;
        private System.Windows.Forms.Button BtnResetSearch;
    }
}

