﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;

using Newtonsoft.Json;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;

using static GoogleSheetGUI.Constants;
using static GoogleSheetGUI.Globals;
using static GoogleSheetGUI.Globals.Google;

/* GoogleSheetGUI, Form1.cs
 * A simple .NET GUI with CRUD Operations for Google Sheet Documents
 * Kai Sackl, 12.12.2019
 */

namespace GoogleSheetGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            NewRowsCount = 0;
            DgvPopulated = false;
            SpreadsheetList = new List<UserSpreadsheet>();
            NewCellDatas = new List<NewCellData>();
            SpreadsheetList = ReadJsonFile($"{UserdataDir}{SpreadsheetsListFileName}");
            AddUserSpreadsheetsToCombobox(SpreadsheetList);
        }

        #region BtnGetData Events
        private void BtnGetData_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(AuthDir))
                Directory.CreateDirectory(AuthDir);

            UserCredential credential = CreateUserCredential();
            Service = CreateSheetsAPIService(credential);

            // Define request parameters
            string spreadsheetId = TbxSpreadsheetID.Text;
            string range = TbxRange.Text;

            string spreadsheetTitle = GetTitleOfSpreadsheet(spreadsheetId, Service);
            Text = $"GoogleSheetGUI: {spreadsheetTitle}";

            SpreadsheetsResource.ValuesResource.GetRequest request =
                    Service.Spreadsheets.Values.Get(spreadsheetId, range);

            DgvPopulated = false;
            PrintSheetToDataGridView(request);
            DgvPopulated = true;

            // ---------- Add spreadsheet to list ----------
            UserSpreadsheet userSpreadsheet = new UserSpreadsheet(spreadsheetTitle, spreadsheetId, range);
            AddUserSpreadsheetsToList(userSpreadsheet);

            // Sort spreadsheet list alphabetically
            SpreadsheetList.Sort((x, y) => x.Title.CompareTo(y.Title));

            WriteJsonFile($"{UserdataDir}{SpreadsheetsListFileName}", SpreadsheetList);

            CbxSpreadsheet.Items.Clear();
            AddUserSpreadsheetsToCombobox(SpreadsheetList);
        }
        #endregion

        #region BtnPushToSheet Events
        private void BtnPushToSheet_Click(object sender, EventArgs e)
        {
            if (NewRowsCount > 0)
            {
                //CreateNewEntries();
                UpdateEntriesCountLabel();
                NewRowsCount = 0;
            }

            if (NewCellDatas.Count > 0)
            {
                WriteToCell();
                NewCellDatas.Clear();
            }
        }
        #endregion

        #region BtnSearch Events
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            string searchText = TbxSearch.Text.ToLower().Trim();

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                for (int i = 0; i < DgvSheet.Rows.Count - 1; i++)
                {
                    DgvSheet.Rows[i].Visible = true;
                    if (DgvSheet.Rows[i].Cells[0].Value != null && !DgvSheet.Rows[i].Cells[0].Value.ToString().ToLower().Contains(searchText))
                        DgvSheet.Rows[i].Visible = false;
                }
                UpdateEntriesCountLabel();
                BtnResetSearch.Visible = true;
            }
        }
        #endregion

        #region BtnResetSearch Events
        private void BtnResetSearch_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DgvSheet.Rows.Count - 1; i++)
                DgvSheet.Rows[i].Visible = true;
            TbxSearch.Text = "";
            UpdateEntriesCountLabel();
            BtnResetSearch.Visible = false;
        }
        #endregion

        #region TbxSearch Events
        private void TbxSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
                BtnSearch.PerformClick();
        }
        #endregion

        #region DataGridView Events
        private void DgvSheet_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!DgvPopulated)
                return;

            int? rowIndex = e?.RowIndex;
            int? colIndex = e?.ColumnIndex;

            if (rowIndex.HasValue && colIndex.HasValue)
            {
                DataGridView dgv = (DataGridView)sender;
                object newCellValue = dgv?.Rows[rowIndex.Value]?.Cells[colIndex.Value]?.Value;
                if (newCellValue != null)
                {
                    NewCellData newCellData = new NewCellData(0, rowIndex.Value, colIndex.Value, newCellValue.ToString());
                    NewCellDatas.Add(newCellData);
                }
                else
                {
                    NewCellData newCellData = new NewCellData(0, rowIndex.Value, colIndex.Value, "");
                    NewCellDatas.Add(newCellData);
                }
            }
        }

        private void DgvSheet_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            NewRowsCount++;
        }
        #endregion

        #region CbxSpreadsheet
        private void CbxSpreadsheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            TbxSpreadsheetID.Text = SpreadsheetList[CbxSpreadsheet.SelectedIndex].SpreadsheetId;
            TbxRange.Text = SpreadsheetList[CbxSpreadsheet.SelectedIndex].Range;
        }
        #endregion

        #region DataGridView Functions
        /// <summary>
        /// Prints the requested sheet to the DataGridView
        /// </summary>
        /// <param name="request"></param>
        private void PrintSheetToDataGridView(SpreadsheetsResource.ValuesResource.GetRequest request)
        {
            try
            {
                // Shows the Sheet in the DataGridView
                ValueRange response = request.Execute();
                IList<IList<Object>> values = response.Values;
                if (values != null && values.Count > 0)
                {
                    ClearDataGridView(DgvSheet);

                    for (int i = 0; i < values[0].Count; i++)
                        DgvSheet.Columns.Add(values[0][i].ToString(), values[0][i].ToString());

                    for (int i = 1; i < values.Count; i++)
                    {
                        DgvSheet.Rows.Add();
                        for (int j = 0; j < values[i].Count; j++)
                            DgvSheet.Rows[i - 1].Cells[j].Value = values[i][j];
                    }

                    DgvSheet.RowsAdded += new DataGridViewRowsAddedEventHandler(DgvSheet_RowsAdded);
                    UpdateEntriesCountLabel();
                    NewRowsCount = 0;
                }
                else
                {
                    MessageBox.Show("No Data found");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error!");
            }
        }

        /// <summary>
        /// Gets the new entries (rows) from the DataGridView and writes it to the spreadsheet
        /// </summary>
        private void CreateNewEntries()
        {
            ValueRange newEntries = new ValueRange();
            List<object> entry = new List<object>(); // Holds values of all cells of one entry
            List<IList<object>> newEntryCollection = new List<IList<object>>(); // Holds all new entries

            int actualDgvRowCount = DgvSheet.Rows.Count - NewRowsCount - 1;

            for (int i = 0; i < NewRowsCount; i++)
            {
                entry = new List<object>();
                for (int j = 0; j < DgvSheet.Rows[actualDgvRowCount + i].Cells.Count; j++)
                    entry.Add(DgvSheet.Rows[actualDgvRowCount + i].Cells[j].Value);
                newEntryCollection.AddRange(new List<IList<object>> { entry });
            }

            newEntries.Values = newEntryCollection;

            var appendRequest = Service.Spreadsheets.Values.Append(newEntries, TbxSpreadsheetID.Text, TbxRange.Text);
            appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            try
            {
                _ = appendRequest.Execute();
                NewRowsCount = 0;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error!");
            }
        }

        /// <summary>
        /// Writes string to cell
        /// </summary>
        private void WriteToCell()
        {
            List<Request> requests = new List<Request>();

            foreach (NewCellData cell in NewCellDatas)
            {
                CellData cellData = new CellData
                { 
                    UserEnteredValue = new ExtendedValue { StringValue = cell.Value }
                };

                List<CellData> cellDatas = new List<CellData> { cellData };
                RowData rowData = new RowData { Values = cellDatas };
                List<RowData> rowDatas = new List<RowData> { rowData };

                UpdateCellsRequest updateCellsRequest = new UpdateCellsRequest()
                {
                    Rows = rowDatas,
                    Fields = "*",
                    Range = new GridRange()
                    {
                        SheetId = cell.SheetId,
                        StartRowIndex = cell.StartRowIndex,
                        EndRowIndex = cell.EndRowIndex,
                        StartColumnIndex = cell.StartColumnIndex,
                        EndColumnIndex = cell.EndColumnIndex
                    }
                };

                Request cellUpdate = new Request { UpdateCells = updateCellsRequest };
                requests.Add(cellUpdate);
            }

            BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest
            {
                Requests = requests
            };

            SpreadsheetsResource.BatchUpdateRequest batchUpdateRequest = Service.Spreadsheets.BatchUpdate(requestBody, TbxSpreadsheetID.Text);
            
            try
            {
                _ = batchUpdateRequest.Execute();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error!");
            }
        }
        #endregion

        #region Google API Functions
        /// <summary>
        /// Creates an credentials.json file and returns the credentials
        /// </summary>
        /// <returns>UserCredential</returns>
        private UserCredential CreateUserCredential()
        {
            UserCredential credential = null;
            try
            {
                using (var stream = new FileStream($"{AuthDir}{CredentialsFileName}", FileMode.Open, FileAccess.Read))
                {
                    // The file token.json stores the user's access and refresh tokens, and is created
                    // automatically when the authorization flow completes for the first time.
                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        Scopes,
                        "GoogleSheetGUI",
                        CancellationToken.None,
                        new FileDataStore(AuthDir, true)).Result;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error!");
            }
            return credential;
        }

        /// <summary>
        /// Creates the Google Sheets API service
        /// </summary>
        /// <param name="credential"></param>
        /// <returns>SheetsService</returns>
        private SheetsService CreateSheetsAPIService(UserCredential credential)
        {
            SheetsService service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            return service;
        }
        #endregion

        #region JSON Functions
        /// <summary>
        /// Deserializes a JSON file to a list
        /// </summary>
        /// <param name="jsonFilePath"></param>
        /// <returns>List of UserSpreadsheet</returns>
        private List<UserSpreadsheet> ReadJsonFile(string jsonFilePath)
        {
            if (File.Exists(jsonFilePath))
                return JsonConvert.DeserializeObject<List<UserSpreadsheet>>(File.ReadAllText(jsonFilePath));
            return new List<UserSpreadsheet>();
        }

        /// <summary>
        /// Serialize a List to a JSON file
        /// </summary>
        /// <param name="jsonFilePatch"></param>
        /// <param name="list"></param>
        private void WriteJsonFile(string jsonFilePatch, List<UserSpreadsheet> list)
        {
            if (!Directory.Exists(UserdataDir))
                Directory.CreateDirectory(UserdataDir);

            File.WriteAllText(jsonFilePatch, JsonConvert.SerializeObject(list, Formatting.Indented));
        }
        #endregion

        #region Helper Functions
        /// <summary>
        /// Clears passed DataGridView
        /// </summary>
        /// <param name="dgv">DataGridView</param>
        private void ClearDataGridView(DataGridView dgv)
        {
            dgv.DataSource = null;
            dgv.Rows.Clear();
            dgv.Columns.Clear();
        }

        /// <summary>
        /// Updates the entries count label
        /// </summary>
        private void UpdateEntriesCountLabel()
        {
            int visibleRowCount = DgvSheet.Rows.GetRowCount(DataGridViewElementStates.Visible);
            if (visibleRowCount >= 1)
                LblEntries.Text = $"Entries: {visibleRowCount - 1}";
            else
                LblEntries.Text = "Entries: 0";
        }

        /// <summary>
        /// Gets the title of the spreadsheet with the given ID
        /// </summary>
        /// <param name="spreadsheetId"></param>
        /// <param name="sheetsService"></param>
        /// <returns>Title of spreadsheet</returns>
        private string GetTitleOfSpreadsheet(string spreadsheetId, SheetsService sheetsService)
        {
            Spreadsheet spreadsheet = sheetsService.Spreadsheets.Get(spreadsheetId).Execute();
            return spreadsheet.Properties.Title;
        }

        /// <summary>
        /// Adds all UserSpreadsheets to the combobox
        /// </summary>
        private void AddUserSpreadsheetsToCombobox(List<UserSpreadsheet> list)
        {
            foreach (UserSpreadsheet spreadsheet in list)
                CbxSpreadsheet.Items.Add(spreadsheet.Title);
        }

        /// <summary>
        /// Adds a UserSpreadsheet to the spreadsheet list, if not already in list
        /// </summary>
        /// <param name="userSpreadsheet"></param>
        private void AddUserSpreadsheetsToList(UserSpreadsheet userSpreadsheet)
        {
            if (!SpreadsheetList.Exists(spreadsheet => spreadsheet.SpreadsheetId == TbxSpreadsheetID.Text))
                SpreadsheetList.Add(userSpreadsheet);
        }
        #endregion
    }
}
