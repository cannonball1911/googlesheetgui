﻿/* GoogleSheetGUI, Constants.cs
 * Contains some constants
 * Kai Sackl, 19.02.2020
 */

namespace GoogleSheetGUI
{
    internal static class Constants
    {
        internal const string AuthDir = @".\auth\";
        internal const string UserdataDir = @".\userdata\";
        internal const string SpreadsheetsListFileName = "spreadsheets_list.json";
        internal const string CredentialsFileName = "credentials.json";
    }
}
