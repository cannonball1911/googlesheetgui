﻿/* GoogleSheetGUI, UserSpreadsheet.cs
 * Holds information of user spreadsheets
 * Kai Sackl, 19.01.2020
 */

namespace GoogleSheetGUI
{
    public class UserSpreadsheet
    {
        public string Title { get; }
        public string SpreadsheetId { get; }
        public string Range { get; }

        public UserSpreadsheet(string title, string spreadsheetId, string range)
        {
            Title = title;
            SpreadsheetId = spreadsheetId;
            Range = range;
        }
    }
}
